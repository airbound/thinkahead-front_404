
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Home } from '../views/Home';
import { Game } from '../views/Game';


export const Router: React.FC<{}> = () => {
    return (
        <BrowserRouter>
            <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/game" component={Game} />
                  
            </Switch>
        </BrowserRouter>
    )
};