import styled from "styled-components";

interface IProps {
    color?: string;
}
export const Button = styled.button`
    border: 0px;
    box-shadow: none;
    color: #ffffff;
    background-color: ${(props: IProps) => props.color || "#342135"};
    height: 40px;
    width: 400px;
    border-radius: 5px;
    font-family: Roboto-Thin;
    font-size: 25px;
`;