import styled from 'styled-components';

export const Grid = {

    Row: styled.div`
        display: flex;
        justify-content:center;
        border: 1px solid #e3e3e3

    `,
    Container: styled.div`
        display:flex;
        flex-wrap: wrap;
        flex-direction: column;
        width:70%;
        margin: 0px auto;
        border: 1px solid #e3e3e3
    `,    
}