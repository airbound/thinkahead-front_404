import React from "react";
import styled from "styled-components";

interface ICaseProps  {
    number?: number;
    active: boolean;
    selected?: boolean
}
export const Case: React.FC<ICaseProps> = ({active, number, selected}) => {
    const Container =  styled.button`
        width: 100%;
        height: 4rem;
        border: 1px solid #e3e3e3;
        color: #ffffff;
        background-color: ${(props: ICaseProps) => props.active ? "#1AC035" : "transparent"};
        border-radius: 0px;
        

    `;
    return <Container active={active} disabled={selected}>{number}</Container>
};