import React from "react";
import styled from "styled-components";
interface IProps {
  name?: string;
  score?: number;
  active?: boolean;
}

export const PlayerCard: React.FC<IProps> = ({ active, score, name }) => {
  const PlayerCardContainer = styled.div`
    background-color: #044561;
    color: #ffffff;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    padding-top: 0.5em;
    padding-left: 1.5em;
    padding-right: 1.5em;
    padding-bottom: 0.5em;
    min-height: 180px;
    min-width: 100px;
    margin: 0px 20px;
    border: ${(prop: IProps) => prop.active ? "5px solid #ffffff" : "none"};
  `;
  return (
    <PlayerCardContainer active={active}>
      <h2>{name}</h2>
      <span>Score: {score}</span>
    </PlayerCardContainer>
  );
};
