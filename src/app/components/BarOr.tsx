import React from "react";
import styled from "styled-components";

const BarOrContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  color: white;
  margin-top: 40px;
  margin-bottom: 40px;
`;

const Bar = {
  backgroundColor: "white",
  height: "1px",
  width: "180px",
  marginTop: "15px",
};

const Text = {
  marginLeft: "5px",
  marginRight: "5px",
  fontSize: "30px"
};

const Flex = {
  display: "flex",
  verticalAlign: "middle",
};

export const BarOr: React.FC<{}> = () => {
  return (
    <BarOrContainer>
      <div style={Flex}>
        <div style={Bar}></div>
        <div style={Text}>OR</div>
        <div style={Bar}></div>
      </div>
    </BarOrContainer>
  );
};
