import React from "react"
import styled from "styled-components";
import { PlayerCard } from "../components/Game/PlayerCard";
import { Grid } from '../components/Game/Grid/index';
import { Case } from "../components/Game/Grid/Case";

const GameContainer = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
`;
export const Game: React.FC<{}> = () => {
    return (
        <GameContainer>
            <PlayerCard active={true} name="Tauma" score={20} />
            <Grid.Container>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
              <Grid.Row>
                  <Case number={1} active={true} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
                  <Case number={1} active={false} selected={false} />
              </Grid.Row>
            </Grid.Container>
            <PlayerCard  name="Tauma" score={20} />
        </GameContainer>
    )
};