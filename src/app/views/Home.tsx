import React from "react";
import styled from "styled-components";
import { Button } from "../components/Button";
import { BarOr } from "../components/BarOr";
import Modal from "react-modal";

const HomeContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: white;
`;

const Title = {
  marginBottom: "50px",
  fontSize: "50px",
};

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "500px",
    height: "250px",
    textAlign: "center",
    backgroundColor: "#132444"
  },
};

const InputStyle = {
  border: "0px",
  boxShadow: "none",
  color: "#fffffff",
  height: '40px',
  width: '395px',
  borderRadius: '5px',
  fontFamily: 'Roboto-Thin',
  fontSize: '25px',
  marginTop: '40px',
  marginBottom: '40px'
}

const SubTitle = {
  color: "#ffffff"
}

export const Home: React.FC<{}> = () => {
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <HomeContainer>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <h2 style={SubTitle}>Enter code</h2>
        <input style={InputStyle} type="text"></input>
        <Button>Join Room</Button>
      </Modal>
      <h1 style={Title}>Think A Head</h1>
      <Button onClick={openModal}>Join Room</Button>
      <BarOr />
      <Button color="#181161">Create Room</Button>
    </HomeContainer>
  );
};
